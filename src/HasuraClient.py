import json
import httpx
import traceback

class HasuraClient:
    def __init__(self, endpoint, secret):
        self.endpoint = endpoint
        self.secret = secret
    
    async def mutate(self, data, period):
        query = """
            mutation insert_ratings_""" + period + """($object: ratings_""" + period + """_insert_input!) {
                insert_ratings_""" + period + """_one(
                    object: $object, 
                    on_conflict: {
                        constraint: ratings_""" + period + """_pkey, update_columns: [
                            values_1_count, values_2_count, values_3_count, values_4_count, values_5_count, values_avg
                        ]
                    }
                ) {
                    """ + ('day' if period == 'daily' else 'month') + """
                    entrypoint_id
                }
            }
        """
        variables = {'object': json.loads(data)[0]}
        return await self.save(query, variables)

    async def query(self, data):
        query = """
            query getEntrypointAndTenantNames {
                entrypoints(where: {id: {_eq: \"""" + data['entrypoint_id'] + """\"}}) {
                    name
                    entrypoints_to_tenants {
                        name
                    }
                }
            }        
        """
        return await self.get(query)

    async def save(self, query, variables):
        headers = {
            'Content-Type': 'application/json',
            'X-Hasura-Admin-Secret': self.secret
        }
        async with httpx.AsyncClient() as client:
            try:
                response = await client.post(self.endpoint, json={'query': query, 'variables': variables}, headers=headers)
                if response.status_code == 200:
                    print(response.json())
                    return response.json()
                else:
                    raise Exception("Query failed to run by returning code of {}. {}".format(response.status_code, query))
            except Exception:
                traceback.print_exc()
                print('An error occurre while contacting Hasura at ' + self.endpoint)

    async def get(self, query):
        headers = {
            'Content-Type': 'application/json',
            'X-Hasura-Admin-Secret': self.secret
        }
        try:
            async with httpx.AsyncClient() as client:
                response = await client.post(self.endpoint, json={'query': query}, headers=headers)
                if response.status_code == 200:
                    print(response.json())
                    return response.json()
                else:
                    raise Exception("Query failed to run by returning code of {}. {}".format(response.status_code, query))
        except Exception:
            traceback.print_exc()
            print('An error occurre while contacting Hasura at ' + self.endpoint)

    