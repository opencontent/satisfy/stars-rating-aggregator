from random import randint
from typing import Set, Any
from fastapi import FastAPI, status
from numpy import unravel_index
from pydantic import BaseModel
from itsdangerous import exc
from kafka import TopicPartition
from datetime import timedelta, datetime
from prometheus_client import Counter
from starlette.responses import RedirectResponse
from starlette_exporter import PrometheusMiddleware, handle_metrics
from pythonjsonlogger import jsonlogger
from HasuraClient import HasuraClient
from dotenv import load_dotenv
from signal import signal, SIGINT
from sys import exit

import uvicorn
import aiokafka
import asyncio
import json
import logging
import os
import time
import traceback
import pandas as pd


# instantiate the API
app = FastAPI()

load_dotenv()

# global variables
consumer_task = None
consumer = None
#_state = 0

# env variables
KAFKA_TOPIC = os.getenv('KAFKA_TOPIC', default='satisfy-ratings-raw')
KAFKA_CONSUMER_GROUP_PREFIX = os.getenv('KAFKA_CONSUMER_GROUP_PREFIX', default='satisfy-rating-aggregator')
KAFKA_BOOTSTRAP_SERVERS = os.getenv('KAFKA_BOOTSTRAP_SERVERS', default='localhost:29092')
FASTAPI_APP_HOST = os.getenv('FASTAPI_APP_HOST', default='0.0.0.0')
FASTAPI_APP_PORT = os.getenv('FASTAPI_APP_PORT', default=5005)
PROMETHEUS_JOB_NAME = os.getenv('PROMETHEUS_JOB_NAME', default='satisfy_rating_aggregator')
HASURA_ENDPOINT = os.getenv('HASURA_ENDPOINT', default='https://satisfy.hasura.app/v1/graphql')
HASURA_SECRET = os.getenv('HASURA_SECRET')

REDIRECT_COUNT = Counter("satisfy_rating_total", "Count of ratings per star", ("star_rating", "tenant", "entrypoint"))
df_aggregate_daily = pd.DataFrame(columns=['day', 'entrypoint_id', 'values_1_count', 'values_2_count', 'values_3_count', 'values_4_count', 'values_5_count', 'values_avg', 'week', 'month', 'year'])
entrypoints_and_tenants_list = []

app.add_middleware(PrometheusMiddleware, app_name=PROMETHEUS_JOB_NAME, prefix=PROMETHEUS_JOB_NAME)

log = logging.getLogger(__name__)
class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        message = log_record['message']
        message = message.replace("\"", "")
        message = message.split(" ")
        log_record['host'] = message[0]
        log_record['method'] = message[2]
        log_record['resource'] = message[3]
        log_record['http_version'] = message[4]
        log_record['status_code'] = message[5]
        log_record.pop('message', None)  

class Status(BaseModel):
    status: str

class HealthCheck(BaseModel):
    healthcheck: str

@app.on_event("startup")
async def startup_event():
    log.info('Initializing API ...')
    logger = logging.getLogger('uvicorn.access')
    logHandler = logging.StreamHandler()
    formatter = CustomJsonFormatter('%(asctime)s %(message)s')
    logHandler.setFormatter(formatter)
    logger.addHandler(logHandler)

    await initialize()
    await consume()

@app.on_event("shutdown")
async def shutdown_event():
    log.info('Shutting down API')
    consumer_task.cancel()
    await consumer.stop()

# @app.get('/status', response_model=Status, status_code=status.HTTP_200_OK)
# async def perform_healthcheck():
#     return {'status': 'Everything OK!'}

@app.get('/healthcheck', response_model=HealthCheck, status_code=status.HTTP_200_OK)
async def perform_healthcheck():
    return {'healthcheck': 'Everything OK!'}

async def initialize():
    # loop = asyncio.get_event_loop()
    global consumer
    group_id = f'{KAFKA_CONSUMER_GROUP_PREFIX}'
    log.debug(f'Initializing KafkaConsumer for topic {KAFKA_TOPIC}, group_id {group_id}'
              f' and using bootstrap servers {KAFKA_BOOTSTRAP_SERVERS}')
    consumer = aiokafka.AIOKafkaConsumer(KAFKA_TOPIC,
                                         bootstrap_servers=[broker for broker in KAFKA_BOOTSTRAP_SERVERS.split(',')],
                                         group_id=group_id,
                                         enable_auto_commit=False,
                                         auto_offset_reset='earliest')
    # get cluster layout and join group
    await consumer.start()

    partitions: Set[TopicPartition] = consumer.assignment()
    nr_partitions = len(partitions)
    if nr_partitions != 1:
        log.warning(f'Found {nr_partitions} partitions for topic {KAFKA_TOPIC}. Expecting '
                    f'only one, remaining partitions will be ignored!')
    for tp in partitions:
        # get the log_end_offset
        end_offset_dict = await consumer.end_offsets([tp])
        end_offset = end_offset_dict[tp]

        if end_offset == 0:
            log.warning(f'Topic ({KAFKA_TOPIC}) has no messages (log_end_offset: '
                        f'{end_offset}), skipping initialization ...')
            return

        log.debug(f'Found log_end_offset: {end_offset} seeking to {end_offset-1}')
        # consumer.seek(tp, end_offset-1)
        await consumer.seek_to_beginning(tp)
        msg = await consumer.getone()
        log.info(f'Initializing API with data from msg: {msg}')

        # update the API state
        #_update_state(msg)
        return

async def consume():
    global consumer_task
    consumer_task = asyncio.create_task(send_consumer_message(consumer))

async def send_consumer_message(consumer):
    try:
        # consume messages
        async for msg in consumer:
            rating = json.loads(msg.value)
            print(rating)
            await aggregate_daily(rating)
    except Exception as e:
        traceback.print_exc()
    finally:
        # will leave consumer group; perform autocommit if enabled
        log.warning('Stopping consumer')
        await consumer.stop()
        exit(1)

async def aggregate_daily(rating):
    global df_aggregate_daily

    current_date = rating['timestamp']
    if 'entrypoint-id' in rating:
        if(rating['entrypoint-id'] not in df_aggregate_daily.values):
            week = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
            month = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%m"))
            year = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%G"))

            aggregate_obj = {
                'day': rating['timestamp'], 
                'entrypoint_id': rating['entrypoint-id'],
                'values_1_count': 1 if rating['value'] == 1 else 0,
                'values_2_count': 1 if rating['value'] == 2 else 0,
                'values_3_count': 1 if rating['value'] == 3 else 0,
                'values_4_count': 1 if rating['value'] == 4 else 0,
                'values_5_count': 1 if rating['value'] == 5 else 0,
                'values_avg': rating['value'],
                'week': week,
                'month': month,
                'year': year
            } 
            df_aggregate_daily = df_aggregate_daily.append(aggregate_obj, ignore_index=True)   
        elif((datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(df_aggregate_daily['day'][df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].values[0].split('T')[0], '%Y-%m-%d')).days > 0):
            days_difference = (datetime.strptime(current_date.split('T')[0], '%Y-%m-%d') - datetime.strptime(df_aggregate_daily['day'][df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].values[0].split('T')[0], '%Y-%m-%d')).days
            if(days_difference > 1):
                print(df_aggregate_daily)
                print("\n")    
                # send current row to hasura
                await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'), 'daily')
                
                # export aggregation to /metrics for prometheus
                for j in range(1,6):
                    await export_metrics(json.loads(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'))[0], j)

                # delete it from dataframe 
                df_aggregate_daily.drop(df_aggregate_daily[df_aggregate_daily.entrypoint_id == rating['entrypoint-id']].index, inplace=True)

                for i in range(days_difference-1, 0, -1):
                    timestamp = datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d') - timedelta(days=i)
                    week = int(timestamp.strftime("%V")) 
                    month = int(timestamp.strftime("%m"))
                    year = int(timestamp.strftime("%G"))

                    aggregate_obj = {
                        'day': timestamp.isoformat(),
                        'entrypoint_id': rating['entrypoint-id'],
                        'values_1_count': 0,
                        'values_2_count': 0,
                        'values_3_count': 0,
                        'values_4_count': 0,
                        'values_5_count': 0,
                        'values_avg': 0,
                        'week': week,
                        'month': month,
                        'year': year
                    }
                    await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(json.dumps([aggregate_obj]), 'daily')
                    for j in range(1,6):
                        await export_metrics(aggregate_obj, j)
                
                week = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
                month = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%m"))
                year = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%G"))

                # create and add a new one
                aggregate_obj = {
                    'day': rating['timestamp'],
                    'entrypoint_id': rating['entrypoint-id'],
                    'values_1_count': 1 if rating['value'] == 1 else 0,
                    'values_2_count': 1 if rating['value'] == 2 else 0,
                    'values_3_count': 1 if rating['value'] == 3 else 0,
                    'values_4_count': 1 if rating['value'] == 4 else 0,
                    'values_5_count': 1 if rating['value'] == 5 else 0,
                    'values_avg': rating['value'],
                    'week': week,
                    'month': month,
                    'year': year
                }
                df_aggregate_daily = df_aggregate_daily.append(aggregate_obj, ignore_index=True)     
            else:
                print(df_aggregate_daily)
                print("\n")    
                # send current row to hasura
                await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).mutate(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'), 'daily')

                # export aggregation to /metrics for prometheus
                for j in range(1,6):
                    await export_metrics(json.loads(df_aggregate_daily[df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']].to_json(orient='records'))[0], j)

                # delete it from dataframe 
                df_aggregate_daily.drop(df_aggregate_daily[df_aggregate_daily.entrypoint_id == rating['entrypoint-id']].index, inplace=True)
                
                week = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%V")) 
                month = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%m"))
                year = int(datetime.strptime(rating['timestamp'].split('T')[0], '%Y-%m-%d').strftime("%G"))

                # create and add a new one
                aggregate_obj = {
                    'day': rating['timestamp'],
                    'entrypoint_id': rating['entrypoint-id'],
                    'values_1_count': 1 if rating['value'] == 1 else 0,
                    'values_2_count': 1 if rating['value'] == 2 else 0,
                    'values_3_count': 1 if rating['value'] == 3 else 0,
                    'values_4_count': 1 if rating['value'] == 4 else 0,
                    'values_5_count': 1 if rating['value'] == 5 else 0,
                    'values_avg': rating['value'],
                    'week': week,
                    'month': month,
                    'year': year
                }
                df_aggregate_daily = df_aggregate_daily.append(aggregate_obj, ignore_index=True)
        else:
            df_aggregate_daily['values_'+ str(rating['value']) + '_count'][df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']] += 1
            df_aggregate_daily['values_avg'][df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']] = (
                (sum(df_aggregate_daily['values_' + str(i) + '_count'][df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']] * i for i in range(1,6))) / 
                (sum(df_aggregate_daily['values_' + str(i) + '_count'][df_aggregate_daily['entrypoint_id'] == rating['entrypoint-id']] for i in range(1,6)))  
            )

app.add_route("/metrics", handle_metrics)

async def export_metrics(daily_aggregation, star_index):
    if not any(el.get('entrypoint-id') == daily_aggregation['entrypoint_id'] for el in entrypoints_and_tenants_list):
        entrypoint_and_tenant_name = await HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query(daily_aggregation)
        if('data' in entrypoint_and_tenant_name and len(entrypoint_and_tenant_name['data']['entrypoints']) != 0):
            REDIRECT_COUNT.labels(
                        star_rating = star_index, 
                        tenant = entrypoint_and_tenant_name['data']['entrypoints'][0]['entrypoints_to_tenants']['name'], 
                        entrypoint = entrypoint_and_tenant_name['data']['entrypoints'][0]['name']).inc(daily_aggregation['values_' + str(star_index) + '_count'])
            entrypoints_and_tenants_list.append({
                'entrypoint-id': daily_aggregation['entrypoint_id'],
                'entrypoint-name': entrypoint_and_tenant_name['data']['entrypoints'][0]['name'],
                'tenant-name': entrypoint_and_tenant_name['data']['entrypoints'][0]['entrypoints_to_tenants']['name'] 
            })
    else:
        REDIRECT_COUNT.labels(
                    star_rating = star_index,
                    tenant = [el['tenant-name'] for el in entrypoints_and_tenants_list if el['entrypoint-id'] == daily_aggregation['entrypoint_id']][0], 
                    entrypoint = [el['entrypoint-name'] for el in entrypoints_and_tenants_list if el['entrypoint-id'] == daily_aggregation['entrypoint_id']][0]).inc(daily_aggregation['values_' + str(star_index) + '_count'])  
    return RedirectResponse(url=FASTAPI_APP_HOST + ":" + str(FASTAPI_APP_PORT), status_code=302)

if __name__ == "__main__":
    uvicorn.run(app, host=FASTAPI_APP_HOST, port=int(FASTAPI_APP_PORT), proxy_headers=True, forwarded_allow_ips='*', access_log=False)