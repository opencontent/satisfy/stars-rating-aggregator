# Stars-rating aggregator

Questo componente è parte di Satisfy, una piattaforma per la rilevazione della Customer Satisfaction mediante un classico sistema a stellette.

## Cosa fa

I rating arrivano su una API apposita e vengono archivati in un topic di Kafka, man mano che arrivano.
La chiave usata nel topic di kafka è l'ID dell'entrypoint.

Questo componente preleva dal topic di Kafka i rating ricevuti e aggrega le statistiche giornaliere in un backend-as-a-service implementato con Hasura.

Viene creato un record per ogni giorno che contiene il numero di stellette ricevute con valore 1, valore 2 etc...

Oltre a questo viene calcolato anche il rating medio di quel giorno.

## Documentazione

Le API disponibili vengono esposte sul path `/docs`

## Monitoring

Il microservizio espone anche in formato prometheus le metriche dei rating che elabora, suddivisi per tenant e entrypoint.

## Deploy

Il microservizio è disponibile come app docker pronta all'uso.

Sul path `/healthcheck` è disponibile una url di controllo per verificare che l'applicazione sta funzionando correttamente.

## Configurazione

Il microservizio è configurabile mediante variabili di ambiente

| Nome                        | Default                               | Required | Description                                                      |
| --------------------------- | ------------------------------------- | -------- | ---------------------------------------------------------------- |
| KAFKA_TOPIC                 | satisfy-ratings-raw                   | Yes      | Topic da cui vengono prelevati i ratings                         |
| KAFKA_CONSUMER_GROUP_PREFIX | group                                 | Yes      | Identificativo del consumer che legge da kafka                   |
| KAFKA_BOOTSTRAP_SERVERS     | localhost:29092                       | Yes      | Endpoint mediante il quale ci si connette a kafka                |
| FASTAPI_APP_HOST            | 0.0.0.0                               | Yes      | Indirizzo al quale si espone il microservizio                    |
| FASTAPI_APP_PORT            | 5005                                  | Yes      | Porta alla quale si espone il microservizio                      |
| PROMETHEUS_JOB_NAME         | satisfy_rating_aggregator             | Yes      | Identificativo delle metriche esposte dal microservizio          |
| HASURA_ENDPOINT             | https://satisfy.hasura.app/v1/graphql | Yes      | Endpoint del db dove vengono salvate le aggregazioni giornaliere |
| HASURA_SECRET                            | **********************                                      | Yes         | Password del db dove vengono salvate le aggregazioni giornaliere                                                                  |


